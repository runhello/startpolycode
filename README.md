# startpolycode

My personal template project and build scripts for working with the Polycode library. The contents of this repository may be considered public domain.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/startpolycode).**
